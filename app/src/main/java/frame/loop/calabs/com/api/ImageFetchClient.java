package frame.loop.calabs.com.api;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;

import frame.loop.calabs.com.constants.Server;

/**
 * Created by Sital Mistry on 4/18/15.
 */
public class ImageFetchClient {
	
	private static volatile AsyncHttpClient sClient = new AsyncHttpClient(Server.SERVER_PORT);
	
	// TODO Create prioritizable and batchable request system
	
	
	public static void get(String endpoint, AsyncHttpResponseHandler callback) {
		get(endpoint, null, null, callback);
	}
	
	public static void get(String endpoint, RequestParams params, AsyncHttpResponseHandler callback) {
		get(endpoint, null, params, callback);
	}
	
	public static void get(String endpoint, Header[] headers, RequestParams params, AsyncHttpResponseHandler callback) {
		sClient.get(null, getAbsoluteUrl(endpoint), headers, params, callback);
	}
	
	public static void post(String endpoint, AsyncHttpResponseHandler callback) {
		post(endpoint, null, callback);
	}
	
	public static void post(String endpoint, RequestParams params, AsyncHttpResponseHandler callback) {
		sClient.post(getAbsoluteUrl(endpoint), params, callback);
	}
	
	public static void put(String endpoint, AsyncHttpResponseHandler callback) {
		put(endpoint, null, callback);
	}
	
	public static void put(String endpoint, RequestParams params, AsyncHttpResponseHandler callback) {
		sClient.put(getAbsoluteUrl(endpoint), params, callback);
	}
	
	private static final String getAbsoluteUrl(String endpoint) {
		return endpoint;
	}

}
