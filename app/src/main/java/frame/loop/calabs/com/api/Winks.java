package frame.loop.calabs.com.api;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.Header;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import frame.loop.calabs.com.constants.Server;
import frame.loop.calabs.com.listeners.DataRequestCallback;
import frame.loop.calabs.com.photoviewer.BuildConfig;

public class Winks {
	
	private static final String TAG = Winks.class.getSimpleName();

	public Winks() {
		// TODO Auto-generated constructor stub
	}
	public static final Winks getInstance() {
		return new Winks();
	}

    public void getMetaDataForFrame(String serialNumber, final DataRequestCallback<JSONObject> callback) {
        LoopHttpClient.get(Server.ENDPOINT_METADATA_FOR_FRAME_FROM_SN + serialNumber, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                if (BuildConfig.DEBUG) {
//                    Log.i(TAG, "Response: " + new String(responseBody));
//                }
                try {
                    JSONObject response = new JSONObject(new String(responseBody));
                    if (response != null) {
                        // Parse out response.
                        callback.onSuccess(response);
                    } else {
                        // TODO: Trigger failure callback
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Error fetching user data based on frame sn", e);
                    // TODO: Trigger failure callback
                }
            }
        });
    }

    public void getWinksForFrame(String frameId, final DataRequestCallback<List<JSONObject>> callback) {
        LoopHttpClient.get(Server.ENDPOINT_WINKS_FROM_FRAMEID + frameId, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
//                if (BuildConfig.DEBUG) {
//                    Log.i(TAG, "Response: " + new String(responseBody));
//                }
                try {
                    JSONArray response = new JSONArray(new String(responseBody));
                    if (response != null) {
                        List<JSONObject> result = new ArrayList<JSONObject>();
                        int len = response.length();
                        if (len > 0) {
                            for (int i = 0; i < len; i++) {
                                try {
                                    result.add(response.getJSONObject(i));
                                } catch (Exception e) {
                                    Log.e(TAG, "Error extracting wink " + i + " from stream, continueing...");
                                }
                            }
                        }
                        callback.onSuccess(result);
                    } else {
                        // TODO: Trigger failure callback
                    }

                } catch (Exception e) {
                    Log.e(TAG, "Error fetching list of winks based on frame id", e);
                }
            }
        });
    }

    public void getWinkUrl(final String winkId, final DataRequestCallback<String> callback) {
        LoopHttpClient.get(Server.ENDPOINT_WINK_IMAGE_URL1 + winkId + Server.ENDPOINT_WINK_IMAGE_URL2, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                String response = new String(responseBody);
                if (BuildConfig.DEBUG) {
                    Log.i(TAG, "Response: " + response);
                }
                if (response != null && response.length() > 0) {
                    callback.onSuccess(response);
                } else {
                    Log.w(TAG, "Failed to fetch url for wink " + winkId + "!!");
                }
            }
        });
    }

    public void getImageFromUrl(final String imgUrl, final DataRequestCallback<Bitmap> callback) {
        ImageFetchClient.get(imgUrl, new AsyncHttpResponseHandler() {

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                if (responseBody.length > 0) {
                    try {
                        Bitmap img = BitmapFactory.decodeByteArray(responseBody, 0, responseBody.length);
                        if (img != null) {
                            if (BuildConfig.DEBUG) {
                                Log.i(TAG, "Retreived Image: " + imgUrl);
                            }
                            callback.onSuccess(img);
                        }
                    } catch (Exception e) {
                        Log.e(TAG, "Failed to decode image response for " + imgUrl);
                    }
                } else {
                    Log.e(TAG, "Failed to get image from " + imgUrl);
                }
            }

        });
    }

}
