package frame.loop.calabs.com.constants;

/**
 * Created by Sital Mistry on 4/18/15.
 */
public interface Server {

    //  Fetch User ID from Frame SN:
    //  http://winkcloud.herokuapp.com/api/frames/sn/02:0F:02:C2:87:3C

    //  Then fetch list of winks from user id
    //  http://winkcloud.herokuapp.com/api/winks/frame/53a13815e4b0b81c27561803

    //  Then fetch a short lived image URL
    //  http://winkcloud.herokuapp.com/api/winks/id/55301e354a0a6d0300000001/url

	public static final String BASE_URL = "http://winkcloud.herokuapp.com/api/";
	public static final int SERVER_PORT = 80;

    public static final String ENDPOINT_METADATA_FOR_FRAME_FROM_SN = "frames/sn/";
    public static final String ENDPOINT_WINKS_FROM_FRAMEID = "winks/frame/";
    public static final String ENDPOINT_WINK_IMAGE_URL1 = "winks/id/";
    public static final String ENDPOINT_WINK_IMAGE_URL2 = "/url";

}
