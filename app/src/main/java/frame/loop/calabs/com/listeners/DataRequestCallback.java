package frame.loop.calabs.com.listeners;

/**
 * Created by Sital Mistry on 4/18/15.
 */
abstract public interface DataRequestCallback<T> {

	abstract public void onSuccess(T result);
	
	abstract public void onFailure(int statusCode, byte[] responseBody);
	
	
}
