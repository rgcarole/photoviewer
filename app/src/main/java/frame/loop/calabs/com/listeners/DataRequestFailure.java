package frame.loop.calabs.com.listeners;

/**
 * Created by Sital Mistry on 4/18/15.
 */
abstract public interface DataRequestFailure {
	
	public void onRequestFailed();
	
	public void onResultSetEmpty();
	
	public void onNetworkConnectionFailed();
	
}
