package frame.loop.calabs.com.photoviewer;

import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.bluetooth.BluetoothHeadset;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioRecord;
import android.media.AudioTrack;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewPropertyAnimator;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.TranslateAnimation;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.hardware.SensorEventListener;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.ISODateTimeFormat;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.UUID;

import frame.loop.calabs.com.api.Winks;
import frame.loop.calabs.com.listeners.DataRequestCallback;


/**
 * Created by Sital Mistry on 4/18/15.
 */


public class PhotoViewer extends Activity {

    public static final String TAG = PhotoViewer.class.getSimpleName();

    private static final int IMAGE_TRANSITION_TIME_MILLIS = 5000;
    private static final int MIN_TIME_BETWEEN_SWIPES_MILLIS = 300; //1300
    private static final int IMAGE_ANIMATION_DURATION_INCOMING_SLIDE = 650;
    private static final int IMAGE_ANIMATION_DURATION_OUTGOING_SLIDE = 375;
    private static final int IMAGE_ANIMATION_DURATION_INCOMING_FADE = 1500;
    private static final int IMAGE_ANIMATION_DURATION_OUTGOING_FADE = 300;
    private static final int TEXT_VIEW_ANIMATION_LENGTH_MILLIS = 800;

    private static final int NUMBER_OF_WINKS_IN_VIEW_FLIPPER = 20;

    private static final int UPDATE_CHECK_FREQUENCY_MILLIS = 5000;

    private static final String GESTURE_CALIBRATION_FILE = "/sys/class/input/input5/calibration";
    private static final String GESTURE_ENABLE_FILE = "/sys/class/input/input5/gesture_enable";
    private static final UUID PERIPHERAL_UUID = UUID.fromString("01010101-0101-0101-0101-010101010101");
    private static final String PERIPHERAL_NAME = "Loop";
    private static final String PERIPHERAL_ADVERTISEMENT_KEY = "kCBAdvDataLocalName";
    private static final UUID SSID_CHARACTERISTIC_UUID = UUID.fromString("01010101-0101-0101-0101-010101524742");
    private static final UUID PASSWORD_CHARACTERISTIC_UUID = UUID.fromString("01010101-0101-0101-0166-616465524742");
    private static final UUID BRIGHTNESS_CHARACTERISTIC_UUID = UUID.fromString("01010101-0101-0101-0101-010101999999");

    private static final String WEBVIEW_URL = "http://192.168.1.99";

    private static final int REQUEST_CODE = 1234;
    private ListView wordsList;

    private ViewFlipper mWinkList;
    private ImageView mScreenDimmer;
    private WebView mWebPort;
    private ImageView mSplash;
    private TextView mStartScreen;
    private TextView mGestureText;
    private ImageView mGestureArrow;
    private TextView mMomentum;
    private FrameLayout mFrameLayout;
    private boolean mArrowSelect;
    private BluetoothAdapter mBluetoothAdapter;
    private BluetoothManager mBluetoothManager;
    private BluetoothDevice mBluetoothDevice;
    private WifiManager mWifiManager;
    private AudioManager mAudioManager;
    private MediaPlayer mMediaPlayer;
    private Intent mSpeechIntent;
    private BluetoothHeadset mBluetoothHeadset;
    private BroadcastReceiver mBroadcastReceiver;
    private Context mContext;

    private boolean mGreetingOn;

    private boolean mIsCountDownOn;
    private boolean mIsStarting;
    private boolean mIsOnHeadsetSco;
    private boolean mIsStarted;

    private AudioTrack mAudioTrack;
    private AudioRecord mAudioRecord;
    private boolean mIsRecording;
    private static final int bufferSize = 200000;

    private SpeechRecognizer mSpeechRecognizer;

    private long mTimeSinceLastFlip;
    private Animation.AnimationListener mSwitchBackToFade;

    private Typeface mTextViewFont;

    private ArrayList<JSONObject> mWinksFetched;

    private long mNewestWink = 0;

    private Runnable mUpdateCheck;
    private Runnable mMaintainWinks;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_viewer);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE);

        mContext = this.getApplicationContext();

        mTextViewFont = Typeface.createFromAsset(getAssets(), "fonts/helvetica-neue-medium.ttf");

        mFrameLayout = (FrameLayout) findViewById(R.id.framelayout_web);
        mFrameLayout.setVisibility(View.VISIBLE);

        mTimeSinceLastFlip = System.currentTimeMillis();
        enableGesture(0);
        calibrateGesture();
        enableGesture(1);
        startGesture();
        //mSplash = (ImageView) findViewById(R.id.splash);
        //mSplash.setBackgroundColor(Color.argb(255, 255, 0, 0));

        /*

        mGestureArrow = (ImageView) findViewById(R.id.gestureArrow);
        mGestureArrow.setVisibility(View.INVISIBLE);
        mGestureArrow.setImageResource(R.drawable.arrow);

        mArrowSelect = true;

        mGestureText = (TextView) findViewById(R.id.gestureNote);
        mGestureText.setVisibility(View.VISIBLE);
        mGestureText.setTypeface(mTextViewFont);
        mGestureText.setText("GESTURE TEST");

        */
        /*
        mMomentum = (TextView) findViewById(R.id.momentum);
        mMomentum.setVisibility(View.INVISIBLE);
        mMomentum.setTypeface(mTextViewFont);
        mMomentum.setText("00:00");
        */
        mGreetingOn = false;

        mStartScreen = (TextView) findViewById(R.id.start_screen);
        mStartScreen.setVisibility(View.VISIBLE);
        mStartScreen.setTypeface(mTextViewFont);
        mStartScreen.setText("Waiting to Connect...");

        while(!checkInternetConnection()) {

           //Log.d("TEST", "waiting for connection");

        }
        mStartScreen.setVisibility(View.INVISIBLE);
        showWebPage();
/*
        //MAC Address
        mWifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        WifiInfo info = mWifiManager.getConnectionInfo();
        String mac_address = info.getMacAddress();
        Log.d("MAC", "address: " + mac_address);
*/

/*


        mWinkList = (ViewFlipper) findViewById(R.id.winklist);
        //mWinkList.setRotation(180);
        mWinkList.setAutoStart(true);
        mWinkList.setFlipInterval(IMAGE_TRANSITION_TIME_MILLIS);
        setAnimationToFade();

        mScreenDimmer = (ImageView) findViewById(R.id.screen_dimmer);
        mScreenDimmer.getBackground().setAlpha(0);

        attachTextViewRevealListener();


        mSwitchBackToFade = new Animation.AnimationListener() {
           @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                setAnimationToFade();
                // Release the listener so the animation can GC
                attachTextViewRevealListener();
                animateTextView();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        };

        AsyncTask runInBg = new AsyncTask() {

            @Override
            protected Object doInBackground(Object[] params) {
                fetchAndPopulateWinks("02:0F:02:C2:87:3C");
                return null;
            }

        };
        runInBg.execute();

        mUpdateCheck = new Runnable() {
            @Override
            public void run() {
                AsyncTask checkForUpdates = new AsyncTask() {
                    @Override
                    protected Object doInBackground(Object[] params) {
                        checkAndPopulateNewWinks("02:0F:02:C2:87:3C");
                        return null;
                    }
                };
                checkForUpdates.execute();
            }
        };
        mWinkList.postDelayed(mUpdateCheck, UPDATE_CHECK_FREQUENCY_MILLIS);

//        mMaintainWinks = new Runnable() {
//            @Override
//            public void run() {
//                AsyncTask maintainWinks = new AsyncTask() {
//                    @Override
//                    protected Object doInBackground(Object[] params) {
//                        rotateWinks();
//                        mWinkList.postDelayed(mMaintainWinks, 8251);
//                        return null;
//                    }
//                };
//                maintainWinks.execute();
//            }
//        };
        // Comment out this line below to kill the image rotation logic
        // mWinkList.postDelayed(mMaintainWinks, 8251);
*/
    }


    public SpannableString updateGreeting() {
        Calendar cal = Calendar.getInstance();
        Log.i(TAG, "Updating?");
        int hour = cal.get(Calendar.HOUR);
        int minute = cal.get(Calendar.MINUTE);
        String curTime = String.format("%d:%02d", hour, minute);
        String greeting = curTime
                + "\n"
                + "Hello, Brian.";
        SpannableString ssGreeting = new SpannableString(greeting);
        ssGreeting.setSpan(new RelativeSizeSpan(5f), 0, 5, 0);
        Log.i(TAG,greeting);
        return ssGreeting;
    }
    public boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) this.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        if (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected()) {
            return true;

        } else {
            return false;
        }
    }
    public void startGesture() {
        SensorManager sManager=(SensorManager)getSystemService(SENSOR_SERVICE);
        Sensor sensor = sManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
        sManager.registerListener(new SensorEventListener() {
            @Override
            public void onSensorChanged(SensorEvent event) {
                // TODO Auto-generated method stub float[] its = event.values;
                float[] its = event.values;
                //if (its != null && event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
                if (its != null) {
                    for (int i =0; i < its.length; i++) {
                        Log.d("proximity test", "its[" + i + "]:" + its[i]);
                        if(i == 0) {
                            /*
                            if(its[i] == 3.0 || its[i] == 5.0 || its[i] == 8.0){
                                // Swipe from left to right, go backwards
                                Log.d("proximity test", "NEXT");
                                showNextWink();
                            } else if(its[i] == 4.0 || its[i] == 6.0 || its[i] == 7.0){
                                // Swipe from right to left, go forwards
                                Log.d("proximity test", "BACK");
                                showPreviousWink();
                            } else if(its[i] == 1.0){
                                showWebPage();
                                //dimmOutScreen();
                            } else if(its[i] == 2.0){
                                //restoreDimmedScreen();
                            }
                            */
                            switch ((int)its[i]) {
                                case 1:
                                    //DOWN
                                    Log.d("proximity test", "1");
                                    mWebPort.loadUrl("javascript:receiveGesture(1)");
                                    break;
                                case 2:
                                    //UP
                                    Log.d("proximity test", "2");
                                    mWebPort.loadUrl("javascript:receiveGesture(2)");
                                    break;
                                case 3:
                                    //LEFT
                                    Log.d("proximity test", "3");
                                    mWebPort.loadUrl("javascript:receiveGesture(3)");
                                    break;
                                case 4:
                                    //RIGHT
                                    Log.d("proximity test", "4");
                                    mWebPort.loadUrl("javascript:receiveGesture(4)");
                                    break;
                                case 5:
                                    Log.d("proximity test", "5");
                                    //drawArrow(315);
                                    //displayCaption("LEFT-UP");
                                    //showNextWink();
                                    //mMomentum.setText(updateGreeting());
                                    //mMomentum.setVisibility(View.VISIBLE);
                                    break;
                                case 6:
                                    Log.d("proximity test", "6");
                                    //drawArrow(45);
                                    //displayCaption("UP-RIGHT");
                                    //showPreviousWink();
                                    //mMomentum.setText(updateGreeting());
                                    //mMomentum.setVisibility(View.VISIBLE);
                                    break;
                                case 7:
                                    Log.d("proximity test", "7");
                                    //displayCaption("RIGHT-DOWN");
                                    //drawArrow(135);
                                    //if(mGreetingOn){
                                    //    mMomentum.setVisibility(View.INVISIBLE);
                                    //    mGreetingOn = false;
                                    //} else {
                                    //    showPreviousWink();
                                    //}
                                    break;
                                case 8:
                                    Log.d("proximity test", "8");
                                    //drawArrow(225);
                                    //displayCaption("DOWN-LEFT");
                                    //if(mGreetingOn){
                                    //    mMomentum.setVisibility(View.INVISIBLE);
                                    //    mGreetingOn = false;
                                    //} else {
                                    //    showNextWink();
                                    //}

                                    break;
                                case 9:
                                    Log.d("proximity test", "9");
                                    //displayCaption("NEAR");
                                    //eraseArrow();
                                    break;
                                case 10:
                                    //displayCaption("FAR");
                                    Log.d("proximity test", "10");
                                    //eraseArrow();
                                    break;
                                case 11:
                                    //displayCaption("CIRCLE_CW");
                                    Log.d("proximity test", "11");
                                    //drawCircular(true);
                                    break;
                                case 12:
                                    //displayCaption("CIRCLE_CCW");
                                    Log.d("proximity test", "12");
                                    //drawCircular(false);
                                    break;
                                default:
                                    //displayCaption("OTHER");
                                    Log.d("proximity test", "Other");
                                    //eraseArrow();
                                    break;
                            }
                        }
                    }
                }
            }

            @Override
            public void onAccuracyChanged(Sensor arg0, int arg1) {
                // TODO Auto-generated method stub
            }
        }, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }
    public void enableGesture(int enable) {

        try {
            FileOutputStream fos = new FileOutputStream(GESTURE_ENABLE_FILE);
            byte[] bytes = new byte[2];
            bytes[0] = (byte)enable;
            bytes[1] = '\n';
            fos.write(bytes);
            fos.close();
            Log.d("Gesture Sensor", "Enabling: " + enable);
            Thread.sleep(1000);
            Log.d("Gesture Sensor", "Enabling of " + enable + " Complete");
        } catch (Exception e) {
            // fail silently
            Log.d("Gesture Sensor", "Failed to Enable " + enable);
        }

    }
    public void calibrateGesture() {

        try {
            FileOutputStream fos = new FileOutputStream(GESTURE_CALIBRATION_FILE);
            byte[] bytes = new byte[2];
            bytes[0] = (byte)2;
            bytes[1] = '\n';
            fos.write(bytes);
            fos.close();
            Log.d("Gesture Sensor", "Calibrating...");
            Thread.sleep(1000);
            Log.d("Gesture Sensor", "Calibration Complete");
        } catch (Exception e) {
            // fail silently
            Log.d("Gesture Sensor", "Failed to Calibrate");
        }

    }

    public void setBrightness(int brightness) {

        //constrain the value of brightness
        if(brightness < 0)
            brightness = 0;
        else if(brightness > 255)
            brightness = 255;


        ContentResolver cResolver = this.getApplicationContext().getContentResolver();
        Settings.System.putInt(cResolver, Settings.System.SCREEN_BRIGHTNESS, brightness);

    }
    public void showWebPage() {

        mWebPort = (WebView) findViewById(R.id.webview);
        mWebPort.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mWebPort.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

            }

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        mWebPort.getSettings().setJavaScriptEnabled(true);
        mWebPort.getSettings().setDomStorageEnabled(true);

        Log.d("TEST", "about to try to connect to web");
        mWebPort.loadUrl(WEBVIEW_URL);

    }
    public void displayCaption(final String note) {
        //mGestureText.setText(note);
    }
    public void drawArrow(final float rotation) {
       /*
        mGestureArrow.setVisibility(View.VISIBLE);
        mGestureArrow.setRotation(rotation);
        if(mArrowSelect) {
            mGestureArrow.setImageResource(R.drawable.arrow);
            mArrowSelect = false;
        } else {
            mGestureArrow.setImageResource(R.drawable.arrow2);
            mArrowSelect = true;
        }
        */
    }
    public void drawCircular(final boolean direction) {
        /*
        mGestureArrow.setVisibility(View.VISIBLE);
        if(mArrowSelect) {
            if(direction){
                mGestureArrow.setImageResource(R.drawable.cw);
            } else {
                mGestureArrow.setImageResource(R.drawable.ccw);
            }
            mArrowSelect = false;
        } else {
            if(direction){
                mGestureArrow.setImageResource(R.drawable.cw2);
            } else {
                mGestureArrow.setImageResource(R.drawable.ccw2);
            }
            mArrowSelect = true;
        }
        */
    }
    public void eraseArrow() {

        //mGestureArrow.setVisibility(View.INVISIBLE);
    }
    public void rotateWinks() {
        final int currentIndex = mWinkList.getDisplayedChild();
        final int childrenCount = mWinkList.getChildCount();
        Log.i(TAG, "RotateWinks: " + currentIndex);
        Log.i(TAG, "Num Children: " + childrenCount);
        if (currentIndex > 10) {
            mWinkList.post(new Runnable() {
                @Override
                public void run() {
                    String lastWinkId = (String) mWinkList.getChildAt(childrenCount - 1).getTag();
//                    for (int i =0; i < 6; i++) {
//                        ((ImageView) (mWinkList.getChildAt(i).findViewById(R.id.image))).setImageDrawable(null);
//                    }
                    mWinkList.removeViews(0, 5);
//                    int adjustedIndex = currentIndex - 5;
//                    if (adjustedIndex < 0) {
//                        adjustedIndex = adjustedIndex + childrenCount;
//                    }
//                    mWinkList.setDisplayedChild(adjustedIndex);
//                    String lastWinkId = (String) mWinkList.getChildAt(adjustedIndex - 1).getTag();
                    int pos = findPosInFullWinkList(lastWinkId);
                    if (pos >= 0) {
                        ArrayList<JSONObject> tmp = mWinksFetched;
                        int len = tmp.size();
                        for(int j = 0; j < 6; j++) {
                            pos++;
                            if (pos >= len) {
                                pos = 0;
                            }
                            Log.i(TAG, "Pos: " + pos);
                            JSONObject winkToFetch = tmp.get(pos);
                            final String winkId = winkToFetch.optString("_id", null);
                            // Only fetch the first 20 images
                            if (winkId != null) {
                                final String imgNote = winkToFetch.optString("note", "");
                                Winks.getInstance().getWinkUrl(winkId, new DataRequestCallback<String>() {
                                    @Override
                                    public void onSuccess(String imgUrl) {
                                        fetchWinkBitmap(imgUrl, imgNote, false, winkId);
                                    }

                                    @Override
                                    public void onFailure(int statusCode, byte[] responseBody) {

                                    }
                                });
                            }
                        }
                    }
                }
            });
        }
    }

    public int findPosInFullWinkList(String winkId) {
        int ret = -1;
        if (mWinksFetched != null && winkId != null) {
            ArrayList<JSONObject> tmp = mWinksFetched;
            int len = tmp.size();
            JSONObject wink;
            for (int i = 0; i < len; i++) {
                wink = tmp.get(i);
                String currentId = wink.optString("_id", null);
                if (currentId != null && winkId.equals(currentId)) {
                    ret = i;
                    break;
                }
            }
        }
        return ret;
    }

    public void dimmOutScreen() {
        ObjectAnimator oa = new ObjectAnimator();
        oa.setTarget(mScreenDimmer.getBackground());
        oa.setPropertyName("Alpha");
        oa.setIntValues(0, 255);
        oa.setDuration(5000);
        oa.setInterpolator(new AccelerateDecelerateInterpolator());
        oa.start();
    }

    public void restoreDimmedScreen() {
        ObjectAnimator oa = new ObjectAnimator();
        oa.setTarget(mScreenDimmer.getBackground());
        oa.setPropertyName("Alpha");
        oa.setIntValues(255, 0);
        oa.setDuration(5000);
        oa.setInterpolator(new AccelerateDecelerateInterpolator());
        oa.start();
    }

    public void attachTextViewRevealListener() {
        mWinkList.getInAnimation().setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                justFlipped();
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                animateTextView();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }

    public void animateTextView() {
        View wink = mWinkList.getCurrentView();
        if (wink != null) {
            TextView v = (TextView)wink.findViewById(R.id.winkNote);
            if (v != null && v.getText() != null && v.getText().length() > 0) {
                v.setVisibility(View.VISIBLE);
                AnimationSet animSet = new AnimationSet(true);
                Animation anim = AnimationUtils.loadAnimation(PhotoViewer.this, android.R.anim.fade_in);
                animSet.addAnimation(anim);
                animSet.setInterpolator(PhotoViewer.this, android.R.interpolator.accelerate_decelerate);
                anim = new TranslateAnimation(0, 0, v.getHeight(), 0);
                animSet.addAnimation(anim);
                animSet.setDuration(TEXT_VIEW_ANIMATION_LENGTH_MILLIS);
                v.startAnimation(animSet);
            }
            int prevChild = mWinkList.getDisplayedChild() - 1;
            if (prevChild < 0) {
                prevChild = mWinkList.getChildCount() - 1;
            }
            mWinkList.getChildAt(prevChild).findViewById(R.id.winkNote).setVisibility(View.INVISIBLE);
        }
    }


    // All of this needs to be refactored... yes its a mess
    public void fetchAndPopulateWinks(String frameSN) {
        if(frameSN == "hardcoded") {
            // Not implemented, app runs out of memory
            //Bitmap fake1 = BitmapFactory.decodeResource(mContext.getResources(), R.drawable.fake1);
            //addNewWink(fake1, "", false, "fake");
        } else {
            Winks.getInstance().getMetaDataForFrame(frameSN, new DataRequestCallback<JSONObject>() {
                @Override
                public void onSuccess(JSONObject result) {
                    if (result != null) {
                        String frameID = result.optString("_id", null);
                        if (frameID != null) {
                            // We have a frame id now fetch the list of winks
                            Winks.getInstance().getWinksForFrame(frameID, new DataRequestCallback<List<JSONObject>>() {
                                @Override
                                public void onSuccess(List<JSONObject> result) {
                                    // Log.d(TAG, "Got " + result.size() + " winks back");
                                    mWinksFetched = (ArrayList) result;
                                    int i = 0;
                                    Winks api = Winks.getInstance();
                                    for (JSONObject wink : result) {
                                        final String winkId = wink.optString("_id", null);
                                        // Only fetch the first 20 images
                                        if (winkId != null && i < NUMBER_OF_WINKS_IN_VIEW_FLIPPER) {
                                            final String imgNote = wink.optString("note", "");
                                            api.getWinkUrl(winkId, new DataRequestCallback<String>() {
                                                @Override
                                                public void onSuccess(String imgUrl) {
                                                    fetchWinkBitmap(imgUrl, imgNote, false, winkId);
                                                }

                                                @Override
                                                public void onFailure(int statusCode, byte[] responseBody) {

                                                }
                                            });
                                        }
                                        String timeStamp = wink.optString("updated_at", null);
                                        if (timeStamp != null) {
                                            try {
                                                // Joda time lib is only used here to parse out the date stamp comming from the server
                                                // Consider having the server switch to time millis
                                                DateTimeFormatter dateParser = ISODateTimeFormat.dateTimeParser();
                                                DateTime date = dateParser.parseDateTime(timeStamp);
                                                long ts = date.getMillis();
                                                if (mNewestWink < ts) {
                                                    mNewestWink = ts;
                                                }
                                            } catch (Exception e) {
                                                Log.e(TAG, "Error parsing date from wink", e);
                                            }
                                        }
                                        i++;
                                    }
                                }

                                @Override
                                public void onFailure(int statusCode, byte[] responseBody) {

                                }
                            });
                        } else {
                            Toast.makeText(PhotoViewer.this, "Failed to fetch frame info", Toast.LENGTH_LONG);
                        }
                    }
                }

                @Override
                public void onFailure(int statusCode, byte[] responseBody) {

                }
            });
        }
    }

    public void checkAndPopulateNewWinks(String frameSN) {

        Winks.getInstance().getMetaDataForFrame(frameSN, new DataRequestCallback<JSONObject>() {
            @Override
            public void onSuccess(JSONObject result) {
                if (result != null) {
                    String frameID = result.optString("_id", null);
                    if (frameID != null) {
                        // We have a frame id now fetch the list of winks
                        Winks.getInstance().getWinksForFrame(frameID, new DataRequestCallback<List<JSONObject>>() {
                            @Override
                            public void onSuccess(List<JSONObject> result) {
                                // Log.d(TAG, "Got " + result.size() + " winks back");
                                mWinksFetched = (ArrayList) result;
                                long updatedNewestTS = mNewestWink;
                                for (JSONObject wink : result) {
                                    String timeStamp = wink.optString("updated_at", null);
                                    if (timeStamp != null) {
                                        try {
                                            // Joda time lib is only used here to parse out the date stamp comming from the server
                                            // Consider having the server switch to time millis
                                            DateTimeFormatter dateParser = ISODateTimeFormat.dateTimeParser();
                                            DateTime date = dateParser.parseDateTime(timeStamp);
                                            long ts = date.getMillis();
                                            if (mNewestWink < ts) {
                                                Log.w(TAG, "Found a new wink!!");
                                                if (updatedNewestTS < ts) {
                                                    updatedNewestTS = ts;
                                                }
                                                final String winkId = wink.optString("_id", null);
                                                // Only fetch the first 20 images
                                                if (winkId != null) {
                                                    final String imgNote = wink.optString("note", "");
                                                    Winks.getInstance().getWinkUrl(winkId, new DataRequestCallback<String>() {
                                                        @Override
                                                        public void onSuccess(String imgUrl) {
                                                            fetchWinkBitmap(imgUrl, imgNote, true, winkId);
                                                        }

                                                        @Override
                                                        public void onFailure(int statusCode, byte[] responseBody) {

                                                        }
                                                    });
                                                }
                                            }
                                        } catch (Exception e) {
                                            Log.e(TAG, "Error parsing date from wink", e);
                                        }
                                    }
                                }
                                mNewestWink = updatedNewestTS;
                                mWinkList.postDelayed(mUpdateCheck, UPDATE_CHECK_FREQUENCY_MILLIS);
                            }

                            @Override
                            public void onFailure(int statusCode, byte[] responseBody) {
                                mWinkList.postDelayed(mUpdateCheck, UPDATE_CHECK_FREQUENCY_MILLIS);
                            }
                        });
                    } else {
                        Toast.makeText(PhotoViewer.this, "Failed to fetch frame info", Toast.LENGTH_LONG);
                    }
                }
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody) {
                mWinkList.postDelayed(mUpdateCheck, UPDATE_CHECK_FREQUENCY_MILLIS);
            }
        });
    }

    /*
    @Override
    public void onResume() {

        // Hide the System UI
        // The UI options currently enabled are represented by a bitfield.
        // getSystemUiVisibility() gives us that bitfield.
        int uiOptions = getWindow().getDecorView().getSystemUiVisibility();
        int newUiOptions = uiOptions;
        if (Build.VERSION.SDK_INT >= 18) {
            newUiOptions ^= View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        }
        getWindow().getDecorView().setSystemUiVisibility(newUiOptions);

    }
    */

    public void fetchWinkImageUrl() {

    }
    // Adding ability to do events based on touch and mouse input
    // use this to trigger actions
    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        boolean ret = super.dispatchTouchEvent(ev);
        Log.i(TAG, "Dispatch Touch Event: " + ev.getButtonState());
        switch (ev.getButtonState()) {
            case MotionEvent.ACTION_UP:
                mWebPort.loadUrl("javascript:receiveGesture(\"M1\")");
                Log.i(TAG, "Left Click");
                break;
            case MotionEvent.ACTION_MOVE:
                mWebPort.loadUrl("javascript:receiveGesture(\"M2\")");
                Log.i(TAG, "Right Click");
                break;
            case MotionEvent.ACTION_OUTSIDE:
                mWebPort.loadUrl("javascript:receiveGesture(\"M3\")");
                Log.i(TAG, "Scrollwheel Click");
                break;
            default:
                break;
        }
        return ret;
    }

    // Wink motions events presently come in as DPAD commands
    // use this to trigger actions
    @Override
    public boolean dispatchKeyEvent(KeyEvent ev) {
        boolean ret = super.dispatchKeyEvent(ev);
        Log.i(TAG, "Dispatch Key Event: " + ev.getKeyCode());
        switch (ev.getKeyCode()) {
            case KeyEvent.KEYCODE_DPAD_LEFT:
                // Swipe from right to left, go forwards (Code 21)
                // showPreviousWink();
                // break;
                mWinkList.removeAllViews();
                System.gc();
                fetchAndPopulateWinks("X02:0F:02:C2:87:3CX");
                break;
            // Disabled to make all events do the same thing
            case KeyEvent.KEYCODE_DPAD_RIGHT:
                // Swipe from left to right, go backwards (Code 22)
                // showNextWink();
                // break;
                mWinkList.removeAllViews();
                System.gc();
                fetchAndPopulateWinks("02:0F:02:C2:87:3C");
                break;
            // Disabled to make all events do the same thing
            case KeyEvent.KEYCODE_DPAD_UP:

                //break;
            // Disabled to make all events do the same thing
            case KeyEvent.KEYCODE_DPAD_DOWN:
                // All four events will funnel into here right now as all break statements are disabled
                showNextWink();
                break;
            case KeyEvent.KEYCODE_EQUALS:
                restoreDimmedScreen();
                break;
            case KeyEvent.KEYCODE_MINUS:
                dimmOutScreen();
                break;
            default:
                break;
        }
        return ret;
    }

    public boolean shouldFlip() {
        boolean ret = false;

        if ((System.currentTimeMillis() - mTimeSinceLastFlip) > MIN_TIME_BETWEEN_SWIPES_MILLIS) {
            ret = true;
        }

        return ret;
    }

    public void justFlipped() {
        mTimeSinceLastFlip = System.currentTimeMillis();
    }

    public void setAnimationToFade() {
        mWinkList.setInAnimation(this, android.R.anim.fade_in);
        mWinkList.getInAnimation().setInterpolator(this, android.R.interpolator.accelerate_decelerate);
        mWinkList.getInAnimation().setDuration(IMAGE_ANIMATION_DURATION_INCOMING_FADE);
        mWinkList.setOutAnimation(this, android.R.anim.fade_out);
        mWinkList.getOutAnimation().setInterpolator(this, android.R.interpolator.accelerate_decelerate);
        mWinkList.getOutAnimation().setDuration(IMAGE_ANIMATION_DURATION_INCOMING_FADE);
    }

    public void setAnimationToSlideForward() {
        int screenWidth = mWinkList.getWidth();

        Animation in = new TranslateAnimation(screenWidth, 0, 0, 0);
        in.setInterpolator(this, android.R.interpolator.accelerate_decelerate);
        in.setDuration(IMAGE_ANIMATION_DURATION_INCOMING_SLIDE);
        mWinkList.setInAnimation(in);

        Animation out = new TranslateAnimation(0, -screenWidth, 0, 0);
        out.setInterpolator(this, android.R.interpolator.accelerate_decelerate);
        out.setDuration(IMAGE_ANIMATION_DURATION_INCOMING_SLIDE);
        out.setAnimationListener(mSwitchBackToFade);
        mWinkList.setOutAnimation(out);
    }

    public void setAnimationToSlideBackward() {
        int screenWidth = mWinkList.getWidth();

        Animation in = new TranslateAnimation(-screenWidth, 0, 0, 0);
        in.setInterpolator(this, android.R.interpolator.accelerate_decelerate);
        in.setDuration(IMAGE_ANIMATION_DURATION_INCOMING_SLIDE);
        mWinkList.setInAnimation(in);

        Animation out = new TranslateAnimation(0, screenWidth, 0, 0);
        out.setInterpolator(this, android.R.interpolator.accelerate_decelerate);
        out.setDuration(IMAGE_ANIMATION_DURATION_INCOMING_SLIDE);
        out.setAnimationListener(mSwitchBackToFade);
        mWinkList.setOutAnimation(out);
    }

    public void showNextWink() {
        mWinkList.post(new Runnable() {
            @Override
            public void run() {
                if (shouldFlip()) {
                    mWinkList.stopFlipping();
                    // mWinkList.clearAnimation();
                    setAnimationToSlideForward();
                    //make new badge disappear
                    View winkView = mWinkList.getCurrentView();
                    ImageView newBadge = (ImageView) winkView.findViewById(R.id.new_badge);
                    newBadge.setVisibility(View.INVISIBLE);
                    ////
                    mWinkList.showNext();
                    mWinkList.startFlipping();
                    Log.i(TAG, "Flipping to Next Image");
                    justFlipped();
                }
            }
        });
    }

    public void showPreviousWink() {
        mWinkList.post(new Runnable() {
            @Override
            public void run() {
                if (shouldFlip()) {
                    mWinkList.stopFlipping();
                    setAnimationToSlideBackward();
                    mWinkList.showPrevious();
                    mWinkList.startFlipping();
                    justFlipped();
                }
            }
        });
    }

    // Used to download wink image from an amazon s3 time limited URL
    public void fetchWinkBitmap(final String imageUrl, final String imgNote, final boolean injectNext, final String winkId) {

        Winks.getInstance().getImageFromUrl(imageUrl, new DataRequestCallback<Bitmap>() {
            @Override
            public void onSuccess(Bitmap result) {
                addNewWink(result, imgNote, injectNext, winkId);
            }

            @Override
            public void onFailure(int statusCode, byte[] responseBody) {

            }
        });
    }

    public void addNewWink(final Bitmap img, final String note, final boolean injectNext, final String winkId) {
        mWinkList.post(new Runnable() {
            @Override
            public void run() {
                LayoutInflater inflator = getLayoutInflater();
                View winkView = inflator.inflate(R.layout.wink_view, null);
                winkView.setTag(winkId);
                TextView txt = (TextView) winkView.findViewById(R.id.winkNote);
                txt.setText(note);
                txt.setVisibility(View.INVISIBLE);
                txt.setTypeface(mTextViewFont);
                ImageView image = (ImageView) winkView.findViewById(R.id.image);
                image.setImageBitmap(img);
                if (injectNext) {
                    ImageView newBadge = (ImageView) winkView.findViewById(R.id.new_badge);
                    newBadge.getBackground().setAlpha(77); // 30% alpha (0-255)
                    newBadge.setVisibility(View.VISIBLE);
                    mWinkList.addView(winkView, mWinkList.getDisplayedChild() + 1);

                    // FInd time next flip will occur:
                    long nextFlip = IMAGE_TRANSITION_TIME_MILLIS - System.currentTimeMillis() + mTimeSinceLastFlip + 1600; // Expected time of next flip plus 250 ms for fudge factor
                    mWinkList.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            // Stop Running
                            mWinkList.stopFlipping();
                        }
                    }, nextFlip);
                } else {
                    mWinkList.addView(winkView);
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        // getMenuInflater().inflate(R.menu.menu_photo_viewer, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
