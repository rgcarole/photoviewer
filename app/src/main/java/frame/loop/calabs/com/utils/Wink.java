package frame.loop.calabs.com.utils;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by Sital Mistry on 4/19/15.
 */
public class Wink {

    /*

    {
        "_id": "55318c8af1c8220300000003",
        "wink": "wink_7BDDDC9D-2ECF-4D77-9754-B0336F8E3181.jpg",
        "type": "photo",
        "user": "52ddcbf30a46de0200000001",
        "size": 161518,
        "note": "Perfect fit ",
        "__v": 0,
        "frames": [
        "53a13815e4b0b81c27561803"
        ],
        "updated_at": "2015-04-17T22:43:22.887Z",
        "created_at": "2015-04-17T22:43:22.887Z"
    },

     */

    private static final String TAG = Wink.class.getSimpleName();

    private String mId, mWinkImageId, mWinkType, mUserId, mFileSize, mWinkNote,
                   mWinkVersion, mUpdatedAt, mCreatedAt;
    private JSONArray mWinkFrames;

    private Wink(String id, String imgId, String type, String userId, String fileSize, String note, String version, String updatedAt, String createdAt, JSONArray frameIds) {
        mId = id;
        mWinkImageId = imgId;
        mWinkType = type;
        mUserId = userId;
        mFileSize = fileSize;
        mWinkNote = note;
        mWinkVersion = version;
        mUpdatedAt = updatedAt;
        mCreatedAt = createdAt;
        mWinkFrames = frameIds;
    }

    public Wink parseFromString(String data) {
        Wink ret = null;
        if (data != null) {
            try {
                JSONObject winkData = new JSONObject(data);
                if (winkData != null) {
                    ret = parseFromJSON(winkData);
                }
            } catch (Exception e) {
                Log.e(TAG, "Error parsing wink data");
            }
        }

        return ret;
    }

    public Wink parseFromJSON(JSONObject winkData) {

        Wink ret = null;

        return ret;
    }

    @Override
    public String toString() {
        String ret = null;

        return ret;
    }


}
